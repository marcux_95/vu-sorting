module SimpleBubbleSorter_top #(
    parameter VALUE_MEM_WIDTH = 208,
    parameter KEY_MEM_WIDTH   = 96,
    parameter NUM_ELEM        = 128
) (
    input clock,
    input reset,

    /* Configuration Interface */
    input                            sorter_conf,
    input                            sorter_start,
    output                           sorter_done,
    output reg                       sorter_dumping,
    output reg [KEY_MEM_WIDTH-1 : 0] sorter_key_dump,

    input [$clog2(NUM_ELEM)-1 : 0] key_mem_config_address,
    input [   KEY_MEM_WIDTH-1 : 0] key_mem_config_data_in,
    input                          key_mem_config_wrt_en,
    input [$clog2(NUM_ELEM)-1 : 0] value_mem_config_address,
    input [ VALUE_MEM_WIDTH-1 : 0] value_mem_config_data_in,
    input                          value_mem_config_wrt_en

);

  /* Key Memory Signals */
  logic [$clog2(NUM_ELEM)-1:0] key_mem_address_A;
  logic [$clog2(NUM_ELEM)-1:0] key_mem_address_B;
  logic [   KEY_MEM_WIDTH-1:0] key_mem_data_in_A;
  logic [   KEY_MEM_WIDTH-1:0] key_mem_data_in_B;
  logic                        key_mem_wrt_en_A;
  logic                        key_mem_wrt_en_B;
  logic [   KEY_MEM_WIDTH-1:0] key_mem_data_out_A;
  logic [   KEY_MEM_WIDTH-1:0] key_mem_data_out_B;

  /* Value Memory Signals */
  logic [$clog2(NUM_ELEM)-1:0] value_mem_address_A;
  logic [$clog2(NUM_ELEM)-1:0] value_mem_address_B;
  logic [   KEY_MEM_WIDTH-1:0] value_mem_data_in_A;
  logic [   KEY_MEM_WIDTH-1:0] value_mem_data_in_B;
  logic                        value_mem_wrt_en_A;
  logic                        value_mem_wrt_en_B;
  logic [   KEY_MEM_WIDTH-1:0] value_mem_data_out_A;
  logic [   KEY_MEM_WIDTH-1:0] value_mem_data_out_B;

  /* Sorter Signals */
  logic [$clog2(NUM_ELEM)-1:0] sorter_key_address_A;
  logic [$clog2(NUM_ELEM)-1:0] sorter_key_address_B;
  logic [   KEY_MEM_WIDTH-1:0] sorter_key_data_in_A;
  logic [   KEY_MEM_WIDTH-1:0] sorter_key_data_in_B;
  logic                        sorter_key_wrt_en_A;
  logic                        sorter_key_wrt_en_B;
  logic [   KEY_MEM_WIDTH-1:0] sorter_key_data_out_A;
  logic [   KEY_MEM_WIDTH-1:0] sorter_key_data_out_B;



  /* Key Memory Structure 
+------------+--------+----------+
| Valid Byte |   Key  | Key Hash |
+------------+--------+----------+
|    8 bit   | 80 bit |   8 bit  |
+------------+--------+----------+
*/

  RAM_2rport_2wport_gen #(
      .ADDR_WIDTH($clog2(NUM_ELEM)),
      .DATA_WIDTH(KEY_MEM_WIDTH),
      .MEMORY_PRIMITIVE("ultra")
  ) KEY_MEM_i (
      .clk(clock),
      .address_A(key_mem_address_A),
      .address_B(key_mem_address_B),

      .data_in_A(key_mem_data_in_A),
      .data_in_B(key_mem_data_in_B),
      .wrt_en_A (key_mem_wrt_en_A),
      .wrt_en_B (key_mem_wrt_en_B),

      .data_out_A(key_mem_data_out_A),
      .data_out_B(key_mem_data_out_B)
  );

  RAM_2rport_2wport_gen #(
      .ADDR_WIDTH($clog2(NUM_ELEM)),
      .DATA_WIDTH(VALUE_MEM_WIDTH),
      .MEMORY_PRIMITIVE("ultra")
  ) VALUE_MEM_i (
      .clk(clock),
      .address_A(value_mem_address_A),
      .address_B(value_mem_address_B),

      .data_in_A(value_mem_data_in_A),
      .data_in_B(value_mem_data_in_B),
      .wrt_en_A (value_mem_wrt_en_A),
      .wrt_en_B (value_mem_wrt_en_B),

      .data_out_A(value_mem_data_out_A),
      .data_out_B(value_mem_data_out_B)
  );

  SimpleBubbleSorter #(

      .VALUE_MEM_WIDTH(VALUE_MEM_WIDTH),
      .KEY_MEM_WIDTH(KEY_MEM_WIDTH),
      .NUM_ELEM(NUM_ELEM)

  ) SimpleBubbleSorter_i (

      .clock(clock),
      .reset(reset),

      .sorter_start(sorter_start),
      .sorter_done(sorter_done),
      .sorter_dumping(sorter_dumping),
      .sorter_key_dump(sorter_key_dump),

      .key_address_A(sorter_key_address_A),
      .key_address_B(sorter_key_address_B),
      .key_data_in_A(sorter_key_data_in_A),
      .key_data_in_B(sorter_key_data_in_B),

      .key_data_out_A(sorter_key_data_out_A),
      .key_data_out_B(sorter_key_data_out_B),
      .key_wrt_en_A  (sorter_key_wrt_en_A),
      .key_wrt_en_B  (sorter_key_wrt_en_B)
  );

  /* Muxing Access Ports */
  always @(*) begin

    key_mem_address_A = 0;
    key_mem_address_B = 0;
    key_mem_wrt_en_A = 0;
    key_mem_wrt_en_B = 0;
    key_mem_data_in_A = 0;
    key_mem_data_in_B = 0;

    value_mem_address_A = 0;
    value_mem_address_B = 0;
    value_mem_wrt_en_A = 0;
    value_mem_wrt_en_B = 0;
    value_mem_data_in_A = 0;
    value_mem_data_in_B = 0;

    sorter_key_data_in_A = key_mem_data_out_A;
    sorter_key_data_in_B = key_mem_data_out_B;

    case ({
      sorter_conf, sorter_start
    })

      2'b1_0: begin
        key_mem_address_A   = key_mem_config_address;
        key_mem_data_in_A   = key_mem_config_data_in;
        key_mem_wrt_en_A    = key_mem_config_wrt_en;
        value_mem_data_in_A = value_mem_config_data_in;
        value_mem_data_in_A = value_mem_config_data_in;
        value_mem_wrt_en_A  = value_mem_config_wrt_en;
      end

      2'b0_1: begin
        key_mem_address_A = sorter_key_address_A;
        key_mem_address_B = sorter_key_address_B;
        key_mem_data_in_A = sorter_key_data_out_A;
        key_mem_data_in_B = sorter_key_data_out_B;
        key_mem_wrt_en_A  = sorter_key_wrt_en_A;
        key_mem_wrt_en_B  = sorter_key_wrt_en_B;
      end

    endcase
  end

endmodule
;
