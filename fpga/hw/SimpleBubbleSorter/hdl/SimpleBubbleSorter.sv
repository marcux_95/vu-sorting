module SimpleBubbleSorter #(
    parameter VALUE_MEM_WIDTH = 208,
    parameter KEY_MEM_WIDTH   = 96,
    parameter NUM_ELEM        = 12
) (
    input clock,
    input reset,

    input                            sorter_start,
    output reg                       sorter_done,
    output reg                       sorter_dumping,
    output reg [KEY_MEM_WIDTH-1 : 0] sorter_key_dump,

    output reg [$clog2(NUM_ELEM)-1 : 0] key_address_A,
    output reg [$clog2(NUM_ELEM)-1 : 0] key_address_B,
    input      [   KEY_MEM_WIDTH-1 : 0] key_data_in_A,
    input      [   KEY_MEM_WIDTH-1 : 0] key_data_in_B,

    output reg [KEY_MEM_WIDTH-1 : 0] key_data_out_A,
    output reg [KEY_MEM_WIDTH-1 : 0] key_data_out_B,
    output reg                       key_wrt_en_A,
    output reg                       key_wrt_en_B

);
  /* Key Memory Structure 
+------------+--------+----------+
| Valid Byte |   Key  | Key Hash |
+------------+--------+----------+
|    8 bit   | 80 bit |   8 bit  |
+------------+--------+----------+
*/

  typedef enum {
    idle,
    data_ready,
    swap,
    increment_address,
    bubble,
    new_iteration,
    done
  } state_encoding_t;

  state_encoding_t                          sorter_state;

  logic                                     swapped;
  logic            [$clog2(NUM_ELEM)-1 : 0] base_address;
  logic                                     wrapped;

  logic            [                 7 : 0] vld_A;
  logic            [                 7 : 0] vld_B;

  assign vld_A = key_data_in_A[95:88];
  assign vld_B = key_data_in_B[95:88];
  assign sorter_key_dump = key_data_in_A;

  always @(posedge clock) begin

    key_address_A <= base_address;
    key_address_B <= base_address + 1;
    key_data_out_A <= 0;
    key_data_out_B <= 0;
    key_wrt_en_A <= 0;
    key_wrt_en_B <= 0;
    sorter_done <= 0;
    wrapped <= 0;
    sorter_dumping <= 0;

    if (reset) begin

      sorter_state <= idle;
      swapped      <= 0;
      base_address <= 0;

    end else begin

      unique case (sorter_state)

        idle: begin
          if (sorter_start) sorter_state <= data_ready;
        end

        data_ready: begin
          wrapped <= 0;
          /* Check if they are both valid */
          if (({vld_A, vld_B}) == 16'hffff) begin
            sorter_state <= swap;
          end else begin
            wrapped <= 1;
            base_address <= 0;
            sorter_state <= new_iteration;
          end
        end

        swap: begin
          /* Check Swap */
          if (({vld_A, vld_B}) == 16'hffff) begin
            if (key_data_in_A[87:8] > key_data_in_B[87:8]) begin
              key_data_out_A <= key_data_in_B;
              key_data_out_B <= key_data_in_A;
              key_wrt_en_A   <= 1;
              key_wrt_en_B   <= 1;
              swapped = 1;
            end
          end
          sorter_state <= increment_address;
        end

        increment_address: begin
          base_address <= base_address + 1;
          sorter_state <= bubble;
        end

        bubble: sorter_state <= data_ready;

        new_iteration: begin
          if (~swapped) sorter_state <= done;
          else begin
            swapped <= 0;
            sorter_state <= bubble;
          end
        end


        done: begin
          sorter_done <= 1;
          if ((base_address < NUM_ELEM - 1) & (vld_A == 8'hff)) begin
            base_address <= base_address + 1;
            sorter_dumping <= 1;
            sorter_done <= 0;
          end
        end
      endcase
    end
  end

endmodule
