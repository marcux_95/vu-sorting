library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

library xpm;
use xpm.vcomponents.all;

entity RAM_2rport_2wport_gen is

  generic (
    ADDR_WIDTH       : integer := 12;
    DATA_WIDTH       : integer := 576;
    MEMORY_PRIMITIVE : string  := "ultra"
  );

  port (
    clk : in std_logic;

    address_A : in std_logic_vector(ADDR_WIDTH - 1 downto 0);
    address_B : in std_logic_vector(ADDR_WIDTH - 1 downto 0);

    data_in_A : in std_logic_vector(DATA_WIDTH - 1 downto 0);
    wrt_en_A  : in std_logic;
    data_in_B : in std_logic_vector(DATA_WIDTH - 1 downto 0);
    wrt_en_B  : in std_logic;

    data_out_A : out std_logic_vector(DATA_WIDTH - 1 downto 0);
    data_out_B : out std_logic_vector(DATA_WIDTH - 1 downto 0)

  );

end RAM_2rport_2wport_gen;

architecture Behavioral of RAM_2rport_2wport_gen is

  signal dbiterrb       : std_logic;
  signal dbiterra       : std_logic;
  signal sbiterrb       : std_logic;
  signal sbiterra       : std_logic;
  signal ena            : std_logic := '1';
  signal enb            : std_logic := '1';
  signal injectdbiterra : std_logic := '0';
  signal injectsbiterra : std_logic := '0';
  signal injectdbiterrb : std_logic := '0';
  signal injectsbiterrb : std_logic := '0';
  signal regceb         : std_logic := '0';
  signal regcea         : std_logic := '0';
  signal rstb           : std_logic := '0';
  signal rsta           : std_logic := '0';
  signal sleep          : std_logic := '0';
  signal wea            : std_logic_vector(0 downto 0);
  signal web            : std_logic_vector(0 downto 0);

  signal data_out_A_s : std_logic_vector(DATA_WIDTH - 1 downto 0);
  signal data_out_B_s : std_logic_vector(DATA_WIDTH - 1 downto 0);

  signal address_A_s : std_logic_vector(ADDR_WIDTH - 1 downto 0);
  signal address_B_s : std_logic_vector(ADDR_WIDTH - 1 downto 0);

begin

  xpm_memory_tdpram_inst : xpm_memory_tdpram
  generic map(
    ADDR_WIDTH_A            => ADDR_WIDTH, -- DECIMAL
    ADDR_WIDTH_B            => ADDR_WIDTH, -- DECIMAL
    AUTO_SLEEP_TIME         => 0, -- DECIMAL
    BYTE_WRITE_WIDTH_A      => DATA_WIDTH, -- DECIMAL
    BYTE_WRITE_WIDTH_B      => DATA_WIDTH, -- DECIMAL
    CASCADE_HEIGHT          => 0, -- DECIMAL
    CLOCKING_MODE           => "common_clock", -- String
    ECC_MODE                => "no_ecc", -- String
    MEMORY_INIT_FILE        => "none", -- String
    MEMORY_INIT_PARAM       => "0", -- String
    MEMORY_OPTIMIZATION     => "true", -- String
    MEMORY_PRIMITIVE        => MEMORY_PRIMITIVE, -- String
    MEMORY_SIZE             => DATA_WIDTH * 2 ** ADDR_WIDTH, -- DECIMAL
    MESSAGE_CONTROL         => 0, -- DECIMAL
    READ_DATA_WIDTH_A       => DATA_WIDTH, -- DECIMAL
    READ_DATA_WIDTH_B       => DATA_WIDTH, -- DECIMAL
    READ_LATENCY_A          => 1, -- DECIMAL
    READ_LATENCY_B          => 1, -- DECIMAL
    READ_RESET_VALUE_A      => "0", -- String
    READ_RESET_VALUE_B      => "0", -- String
    RST_MODE_A              => "SYNC", -- String
    RST_MODE_B              => "SYNC", -- String
    SIM_ASSERT_CHK          => 0, -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
    USE_EMBEDDED_CONSTRAINT => 0, -- DECIMAL
    USE_MEM_INIT            => 1, -- DECIMAL
    WAKEUP_TIME             => "disable_sleep", -- String
    WRITE_DATA_WIDTH_A      => DATA_WIDTH, -- DECIMAL
    WRITE_DATA_WIDTH_B      => DATA_WIDTH, -- DECIMAL
    WRITE_MODE_A            => "no_change", -- String
    WRITE_MODE_B            => "no_change" -- String
  )

  port map(
    dbiterra => dbiterra, -- 1-bit output: Status signal to indicate double bit error occurrence
    -- on the data output of port A.

    dbiterrb => dbiterrb, -- 1-bit output: Status signal to indicate double bit error occurrence
    -- on the data output of port A.

    douta    => data_out_A_s, -- READ_DATA_WIDTH_A-bit output: Data output for port A read operations.
    doutb    => data_out_B_s, -- READ_DATA_WIDTH_B-bit output: Data output for port B read operations.
    sbiterra => sbiterra, -- 1-bit output: Status signal to indicate single bit error occurrence
    -- on the data output of port A.

    sbiterrb => sbiterrb, -- 1-bit output: Status signal to indicate single bit error occurrence
    -- on the data output of port B.

    addra => address_A_s, -- ADDR_WIDTH_A-bit input: Address for port A write and read operations.
    addrb => address_B_s, -- ADDR_WIDTH_B-bit input: Address for port B write and read operations.
    clka  => clk, -- 1-bit input: Clock signal for port A. Also clocks port B when
    -- parameter CLOCKING_MODE is "common_clock".

    clkb => clk, -- 1-bit input: Clock signal for port B when parameter CLOCKING_MODE is
    -- "independent_clock". Unused when parameter CLOCKING_MODE is
    -- "common_clock".

    dina => data_in_A, -- WRITE_DATA_WIDTH_A-bit input: Data input for port A write operations.
    dinb => data_in_B, -- WRITE_DATA_WIDTH_B-bit input: Data input for port B write operations.
    ena  => ena, -- 1-bit input: Memory enable signal for port A. Must be high on clock
    -- cycles when read or write operations are initiated. Pipelined
    -- internally.

    enb => enb, -- 1-bit input: Memory enable signal for port B. Must be high on clock
    -- cycles when read or write operations are initiated. Pipelined
    -- internally.

    injectdbiterra => injectdbiterra, -- 1-bit input: Controls double bit error injection on input data when
    -- ECC enabled (Error injection capability is not available in
    -- "decode_only" mode).

    injectdbiterrb => injectdbiterrb, -- 1-bit input: Controls double bit error injection on input data when
    -- ECC enabled (Error injection capability is not available in
    -- "decode_only" mode).

    injectsbiterra => injectsbiterra, -- 1-bit input: Controls single bit error injection on input data when
    -- ECC enabled (Error injection capability is not available in
    -- "decode_only" mode).

    injectsbiterrb => injectsbiterrb, -- 1-bit input: Controls single bit error injection on input data when
    -- ECC enabled (Error injection capability is not available in
    -- "decode_only" mode).

    regcea => regcea, -- 1-bit input: Clock Enable for the last register stage on the output
    -- data path.

    regceb => regceb, -- 1-bit input: Clock Enable for the last register stage on the output
    -- data path.

    rsta => rsta, -- 1-bit input: Reset signal for the final port A output register
    -- stage. Synchronously resets output port douta to the value specified
    -- by parameter READ_RESET_VALUE_A.

    rstb => rstb, -- 1-bit input: Reset signal for the final port B output register
    -- stage. Synchronously resets output port doutb to the value specified
    -- by parameter READ_RESET_VALUE_B.

    sleep => sleep, -- 1-bit input: sleep signal to enable the dynamic power saving feature.
    wea   => wea, -- WRITE_DATA_WIDTH_A/BYTE_WRITE_WIDTH_A-bit input: Write enable vector
    -- for port A input data port dina. 1 bit wide when word-wide writes
    -- are used. In byte-wide write configurations, each bit controls the
    -- writing one byte of dina to address addra. For example, to
    -- synchronously write only bits [15-8] of dina when WRITE_DATA_WIDTH_A
    -- is 32, wea would be 4'b0010.

    web => web -- WRITE_DATA_WIDTH_B/BYTE_WRITE_WIDTH_B-bit input: Write enable vector
    -- for port B input data port dinb. 1 bit wide when word-wide writes
    -- are used. In byte-wide write configurations, each bit controls the
    -- writing one byte of dinb to address addrb. For example, to
    -- synchronously write only bits [15-8] of dinb when WRITE_DATA_WIDTH_B
    -- is 32, web would be 4'b0010.

  );

  wea(0) <= wrt_en_A;
  web(0) <= wrt_en_B;

  data_out_A <= data_out_A_s;
  data_out_B <= data_out_B_s;

  address_A_s <= address_A;
  address_B_s <= address_B;

end Behavioral;