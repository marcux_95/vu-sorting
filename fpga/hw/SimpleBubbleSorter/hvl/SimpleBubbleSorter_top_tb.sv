module SimpleBubbleSorter_top_tb;
  parameter VALUE_MEM_WIDTH = 208;
  parameter KEY_MEM_WIDTH = 96;
  parameter NUM_ELEM = 128;

  parameter KEY_MEM_INIT_FILE = "/home/marcos/hdl_sources/vu-sorting/fpga/sw/SimpleBubbleSorter/BubbleSorterKeyMem.conf";
  parameter VALUE_MEM_INIT_FILE = "/home/marcos/hdl_sources/vu-sorting/fpga/sw/SimpleBubbleSorter/BubbleSorterValueMem.conf";

  logic                          clock;
  logic                          reset;

  /* Configuration Interface */
  logic                          sorter_conf;
  logic                          sorter_start;
  logic                          sorter_done;
  logic                          sorter_dumping;
  logic [   KEY_MEM_WIDTH-1 : 0] sorter_key_dump;
  logic [$clog2(NUM_ELEM)-1 : 0] key_mem_config_address;
  logic [   KEY_MEM_WIDTH-1 : 0] key_mem_config_data_in;
  logic                          key_mem_config_wrt_en;
  logic [$clog2(NUM_ELEM)-1 : 0] value_mem_config_address;
  logic [ VALUE_MEM_WIDTH-1 : 0] value_mem_config_data_in;
  logic                          value_mem_config_wrt_en;

  logic [   KEY_MEM_WIDTH-1 : 0] test_key_mem_conf        [NUM_ELEM-1:0];
  logic [ VALUE_MEM_WIDTH-1 : 0] test_value_mem_conf      [NUM_ELEM-1:0];

  SimpleBubbleSorter_top #(

      .VALUE_MEM_WIDTH(VALUE_MEM_WIDTH),
      .KEY_MEM_WIDTH(KEY_MEM_WIDTH),
      .NUM_ELEM(NUM_ELEM)
  ) DUT_i (

      .clock(clock),
      .reset(reset),
      .sorter_conf(sorter_conf),
      .sorter_start(sorter_start),
      .sorter_done(sorter_done),
      .key_mem_config_address(key_mem_config_address),
      .key_mem_config_data_in(key_mem_config_data_in),
      .key_mem_config_wrt_en(key_mem_config_wrt_en),
      .value_mem_config_address(value_mem_config_address),
      .value_mem_config_data_in(value_mem_config_data_in),
      .value_mem_config_wrt_en(value_mem_config_wrt_en)

  );

  always #5ns clock = ~clock;

  initial begin
    $timeformat(-9, 2, " ns", 20);
    $readmemh(KEY_MEM_INIT_FILE, test_key_mem_conf);
    $readmemh(VALUE_MEM_INIT_FILE, test_value_mem_conf);
    $display("[%t] ***** SIMULATION STARTS *****", $time);
    clock = 0;
    reset = 1;
    sorter_conf = 0;
    key_mem_config_address = -1;
    key_mem_config_data_in = 0;
    key_mem_config_wrt_en = 0;
    value_mem_config_address = -1;
    value_mem_config_data_in = 0;
    value_mem_config_wrt_en = 0;
    sorter_start = 0;
    @(posedge clock);
    @(posedge clock);
    @(posedge clock);
    @(posedge clock);
    @(posedge clock);
    reset = 0;
    @(posedge clock);
    $display("[%t] ***** Starting Key Memory Configuration ... *****", $time);
    sorter_conf = 1;
    @(posedge clock);
    for (integer i = 0; i < NUM_ELEM; i++) begin
      key_mem_config_address <= key_mem_config_address + 1;
      key_mem_config_data_in <= test_key_mem_conf[i];
      key_mem_config_wrt_en  <= 1;
      @(posedge clock);
    end
    key_mem_config_wrt_en <= 0;
    $display("[%t] ***** Key Memory Configuration Done! *****", $time);
    @(posedge clock);
    @(posedge clock);
    $display("[%t] ***** Starting Value Memory Configuration ... *****", $time);
    sorter_conf = 1;
    @(posedge clock);
    for (integer i = 0; i < NUM_ELEM; i++) begin
      value_mem_config_address <= value_mem_config_address + 1;
      value_mem_config_data_in <= test_value_mem_conf[i];
      value_mem_config_wrt_en  <= 1;
      @(posedge clock);
    end
    value_mem_config_wrt_en <= 0;
    $display("[%t] ***** Value Memory Configuration Done! *****", $time);
    @(posedge clock);
    $display("[%t] ***** Sorter Configuration Done! *****", $time);
    sorter_conf = 0;
    @(posedge clock);
    $display("[%t] ***** Starting Sorter ... *****", $time);
    sorter_start = 1;
  end

  always @(posedge clock) begin

    if (sorter_done) begin
      $display("[%t] ***** Sorter Done *****", $time);

      $finish;
    end
  end


endmodule
;
