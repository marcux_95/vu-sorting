#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "main.h"

void merge_sort(node_t **headRef) {
  node_t *head = *headRef;
  node_t *a;
  node_t *b;

  /* Base case -- length 0 or 1 */
  if ((head == NULL) || (head->next_node == NULL)) {
    return;
  }

  /* Split head into 'a' and 'b' sublists */
  FrontBackSplit(head, &a, &b);

  /* Recursively sort the sublists */
  merge_sort(&a);
  merge_sort(&b);

  /* answer = merge the two sorted lists together */
  *headRef = SortedMerge(a, b);
}

node_t *SortedMerge(node_t *a, node_t *b) {
  node_t *result = NULL;

  /* Base cases */
  if (a == NULL)
    return (b);
  else if (b == NULL)
    return (a);

  /* Pick either a or b, and recur */
  if (memcmp(a->key, b->key, KEY_SIZE) <= 0) {
    result = a;
    result->next_node = SortedMerge(a->next_node, b);
  } else {
    result = b;
    result->next_node = SortedMerge(a, b->next_node);
  }
  return (result);
}

void FrontBackSplit( node_t* source,
                     node_t** frontRef,  node_t** backRef)
{
     node_t* fast;
     node_t* slow;
    slow = source;
    fast = source->next_node;
 
    /* Advance 'fast' two nodes, and advance 'slow' one node_t */
    while (fast != NULL) {
        fast = fast->next_node;
        if (fast != NULL) {
            slow = slow->next_node;
            fast = fast->next_node;
        }
    }
 
    /* 'slow' is before the midpoint in the list, so split it in two
    at that point. */
    *frontRef = source;
    *backRef = slow->next_node;
    slow->next_node = NULL;
}
