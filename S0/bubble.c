#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "main.h"

int bubble_sort(list_t *list) {

  node_t **cur_node = NULL;
  node_t **next_node = NULL;
  node_t *tmp_node = NULL;

  int swapped = 1;
  int iterations = 0;

  while (swapped) {
    iterations++;
    swapped = 0;
    cur_node = &list->head;
    for (int i = 0; i < list->num_elem; i++) {

      if (*cur_node != NULL && (*cur_node)->next_node != NULL) {
        next_node = &(*cur_node)->next_node;
        /* Compare Keys */
        if (memcmp((*cur_node)->key, (*next_node)->key, KEY_SIZE) > 0) {
          /* Swap Elements */
          tmp_node = *cur_node;
          *cur_node = *next_node; /* Attach Head to next Node */
          tmp_node->next_node =
              (*next_node)->next_node; /* Make the old node point to the next's
                                          next one */
          (*cur_node)->next_node =
              tmp_node; /* Attach new current node to the old one */
          swapped++;
        }
        cur_node = &(*cur_node)->next_node;
      }
    }
  }
  return iterations;
}