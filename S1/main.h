#include <stddef.h>
#include <stdio.h>

#define KEY_SIZE 10
#define RECORD_OFFSET (KEY_SIZE + 2)
#define RECORD_SIZE 32
#define VALUE_OFFSET (RECORD_OFFSET + RECORD_SIZE + 2)
#define VALUE_SIZE 52

/* Structs */
struct node {
  char *key;
  char *value;
  char *record_number;
  struct node *next_node;
};

struct list {
  struct node *head;
  int num_elem;
};

/* Typedefs */
typedef struct node node_t;
typedef struct list list_t;

/* Prototypes */
node_t *process_line(char *line, size_t len, FILE *fp);
void dump_list_to_file(list_t *list, FILE *fp);
void merge_sort(node_t **headRef);
node_t *SortedMerge(node_t *a, node_t *b);
void FrontBackSplit(node_t *source, node_t **frontRef, node_t **backRef);
