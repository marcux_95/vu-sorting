#include "main.h"
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define AUDIT if (1)

int main(int argc, char *argv[]) {

  FILE *fp_r;
  FILE *fp_w;
  char *line = NULL;
  size_t len = 0;
  ssize_t read;
  list_t *key_value_list;
  node_t *cur_node = NULL;
  node_t **last_ptr = NULL;
  char *lowest_elem = NULL;
  int call = 0;

  fp_r = fopen(argv[1], "r");
  if (fp_r == NULL) {
    printf("Error opening the file!");
    exit(EXIT_FAILURE);
  }

  fp_w = fopen("./list_dump.txt", "w");

  if (fp_w == NULL) {
    printf("Error opening the file!");
    exit(EXIT_FAILURE);
  }

  /* Materialize linked list */
  key_value_list = malloc(sizeof(list_t));
  key_value_list->num_elem = 0;
  key_value_list->head = NULL;

  last_ptr = &key_value_list->head;

  /* Main Allocation Loop */
  while ((read = getline(&line, &len, fp_r)) != -1) {
    cur_node = process_line(line, len, fp_w);
    cur_node->next_node = NULL;
    *last_ptr = cur_node;
    key_value_list->num_elem++;
    last_ptr = &cur_node->next_node;
  }

  printf("***** STARTING MERGE SORT *****\n");
  merge_sort(&key_value_list->head);
  printf("***** ENDING MERGE SORT *****\n");

  /* Dump Final Sorted List */
  dump_list_to_file(key_value_list, fp_w);

  fclose(fp_r);
  fclose(fp_w);
  if (line)
    free(line);
  exit(EXIT_SUCCESS);
}

node_t *process_line(char *line, size_t len, FILE *fp) {

  node_t *node_i;
  char *record_number;

  int i;

  node_i = malloc(sizeof(node_t));
  if (!node_i) {
    printf("Error Allocating Node\n");
    exit(-1);
  }

  /* Extract Key */
  node_i->key = malloc(KEY_SIZE);
  if (!node_i->key) {
    printf("Error Allocating Key\n");
    exit(-1);
  }
  memcpy(node_i->key, line, KEY_SIZE);

  /* Extract Record Number */
  node_i->record_number = malloc(RECORD_SIZE);
  if (!node_i->record_number) {
    printf("Error Allocating Record Number\n");
    exit(-1);
  }
  memcpy(node_i->record_number, line + RECORD_OFFSET, RECORD_SIZE);

  /* Extract Value */
  node_i->value = malloc(VALUE_SIZE);
  if (!node_i->value) {
    printf("Error Allocating Value\n");
    exit(-1);
  }
  memcpy(node_i->value, line + VALUE_OFFSET, VALUE_SIZE);

  return node_i;
}

void dump_list_to_file(list_t *list, FILE *fp) {
  node_t *last_node = NULL;
  node_t **last_ptr = NULL;

  const char NEW_LINE[1] = {'\n'};
  const char WHITE_SPACE[1] = {0x20};
  const char CARRIAGE_RETURN[1] = {'\r'};

  last_ptr = &list->head;

  for (int i = 0; i < list->num_elem; i++) {
    last_node = (*last_ptr);
    fwrite(last_node->key, 1, KEY_SIZE, fp);
    fwrite(WHITE_SPACE, 1, 1, fp);
    fwrite(WHITE_SPACE, 1, 1, fp);
    fwrite(last_node->record_number, 1, RECORD_SIZE, fp);
    fwrite(WHITE_SPACE, 1, 1, fp);
    fwrite(WHITE_SPACE, 1, 1, fp);
    fwrite(last_node->value, 1, VALUE_SIZE, fp);
    fwrite(CARRIAGE_RETURN, 1, 1, fp);
    fwrite(NEW_LINE, 1, 1, fp);
    last_ptr = &(last_node->next_node);
  }
}
