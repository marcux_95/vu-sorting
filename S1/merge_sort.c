#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "main.h"

void merge_sort(node_t **headRef) {

  pthread_t thread1, thread2;
  node_t *head = *headRef;
  node_t *a;
  node_t *b;
  int iret1, iret2;

  /* Base case -- length 0 or 1 */
  if ((head == NULL) || (head->next_node == NULL)) {
    return;
  }

  /* Split head into 'a' and 'b' sublists */
  FrontBackSplit(head, &a, &b);

  /* Recursively sort the sublists */
  iret1 = pthread_create(&thread1, NULL, merge_sort, &a);
  iret2 = pthread_create(&thread2, NULL, merge_sort, &b);

  pthread_join(thread1, NULL);
  pthread_join(thread2, NULL);
  /* answer = merge the two sorted lists together */
  *headRef = SortedMerge(a, b);
}

node_t *SortedMerge(node_t *a, node_t *b) {
  node_t *result = NULL;

  /* Base cases */
  if (a == NULL)
    return (b);
  else if (b == NULL)
    return (a);

  /* Pick either a or b, and recur */
  if (memcmp(a->key, b->key, KEY_SIZE) <= 0) {
    result = a;
    result->next_node = SortedMerge(a->next_node, b);
  } else {
    result = b;
    result->next_node = SortedMerge(a, b->next_node);
  }
  return (result);
}

void FrontBackSplit(node_t *source, node_t **frontRef, node_t **backRef) {
  node_t *fast;
  node_t *slow;
  slow = source;
  fast = source->next_node;

  /* Advance 'fast' two nodes, and advance 'slow' one node_t */
  while (fast != NULL) {
    fast = fast->next_node;
    if (fast != NULL) {
      slow = slow->next_node;
      fast = fast->next_node;
    }
  }

  /* 'slow' is before the midpoint in the list, so split it in two
  at that point. */
  *frontRef = source;
  *backRef = slow->next_node;
  slow->next_node = NULL;
}
